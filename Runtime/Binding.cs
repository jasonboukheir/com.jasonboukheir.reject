﻿using System;
using Reject.Serialization;

namespace Reject
{
    [Serializable]
    public class Binding
    {
        public SerializableType From;

        [DerivedFrom(nameof(GetFromType))]
        public SerializableType To;

        private Type GetFromType()
        {
            return From.Type;
        }
    }
}
