﻿using System;
using UnityEngine;

namespace Reject.Serialization
{
    [Serializable]
    public class SerializableType : ISerializationCallbackReceiver
    {
        [SerializeField]
        protected string _serializedType;

        public Type Type { get; set; }

        public static Type ToType(string serializedType)
        {
            return Type.GetType(serializedType);
        }

        public static string ToSerializedType(Type type)
        {
            return type?.AssemblyQualifiedName ?? string.Empty;
        }

        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
            Type = ToType(_serializedType);
        }

        void ISerializationCallbackReceiver.OnBeforeSerialize()
        {
            _serializedType = ToSerializedType(Type);
        }
    }
}
