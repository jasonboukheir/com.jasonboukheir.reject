﻿using System;

namespace Reject
{
    public class Factory
    {
        public object Construct(Type type)
        {
            return Activator.CreateInstance(type);
        }
    }
}
