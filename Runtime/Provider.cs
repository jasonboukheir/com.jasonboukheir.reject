﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityObject = UnityEngine.Object;

namespace Reject
{
    [CreateAssetMenu(menuName = "Reject/Provider")]
    public class Provider : ScriptableObject, ISerializationCallbackReceiver
    {
        public UnityObject Injectee;

        [SerializeField]
        private Binding[] bindings = default;

        public Dictionary<Type, Type> Bindings { get; set; }

        public void Inject()
        {
            switch (Injectee)
            {
                case GameObject go: Inject(go); break;
                default: Inject(Injectee); break;
            }
        }

        private void Inject(GameObject go)
        {
            foreach (var component in go.GetComponentsInChildren<MonoBehaviour>())
            {
                Inject(component);
            }
        }

        private void Inject(object injectee)
        {
            foreach (var field in injectee.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
            {
                foreach (var attribute in field.GetCustomAttributes(typeof(InjectAttribute), false))
                {
                    if (Bindings != null && Bindings.TryGetValue(field.FieldType, out var injectedType))
                    {
                        field.SetValue(injectee, Activator.CreateInstance(injectedType));
                    }
                }
            }
        }

        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
            Bindings = bindings?.Where(b => b.From.Type != null).ToDictionary(
                b => b.From.Type,
                b => b.To.Type
            );
        }

        void ISerializationCallbackReceiver.OnBeforeSerialize()
        {

        }
    }
}
