﻿using UnityEngine;

namespace Reject
{
    public class InjectAttribute : PropertyAttribute
    {
        public object Key { get; }

        public InjectAttribute(object key)
        {
            Key = key;
        }

        public InjectAttribute() { }
    }
}
