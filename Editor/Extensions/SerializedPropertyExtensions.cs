﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using UnityEditor;

namespace Reject.Extensions
{
    public static class SerializedPropertyExtensions
    {
        private static readonly Regex ArrayPathRegex = new Regex(
            @"(?<arrayName>\w+)\[(?<index>\d+)\]",
            RegexOptions.Compiled);

        public static object GetValue(this SerializedProperty property,
            Func<IEnumerable<string>, IEnumerable<string>> modifier = null)
        {
            IEnumerable<string> path = property.propertyPath.Replace(".Array.data[", "[").Split('.');
            if (modifier != null)
            {
                path = modifier(path);
            }
            var target = (object)property.serializedObject.targetObject;
            return GetValueRecur(target, path);
        }

        private static object GetValueRecur(object target, IEnumerable<string> path)
        {
            if (target == null) throw new ArgumentNullException(nameof(target));

            var head = path.FirstOrDefault();
            if (head == null)
            {
                return target;
            }

            var arrayMatch = ArrayPathRegex.Match(head);
            if (arrayMatch != null)
            {
                head = arrayMatch.Groups["arrayName"].Value;
                var index = int.Parse(arrayMatch.Groups["index"].Value);
                var field = target.GetType().GetField(head, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
                var array = field.GetValue(target) as IEnumerable;
                target = array.ElementAtOrDefault(index);
            }
            else
            {
                target = target.GetType().GetField(head).GetValue(target);
            }
            return GetValueRecur(target, path.Skip(1));
        }
    }
}
