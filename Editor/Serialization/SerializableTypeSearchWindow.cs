﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Compilation;
using UnityEngine;
using static Reject.Serialization.SerializableType;

namespace Reject.Serialization
{
    public class SerializableTypeSearchWindow : EditorWindow
    {
        private const int MaxResults = 100;

        private GUIStyle selectedStyle;

        private Type baseType;
        private Action<string> onSelected;
        private string searchValue = string.Empty;
        private Type[] searchedTypes;
        private string selectedTypeValue;
        private Vector2 scrollPosition;

        public static SerializableTypeSearchWindow ShowWindow(
            Type baseType,
            string selectedTypeValue,
            Action<string> onSelected
        )
        {
            var window = (SerializableTypeSearchWindow)CreateInstance(typeof(SerializableTypeSearchWindow));
            window.baseType = baseType;
            window.onSelected = onSelected;
            window.selectedTypeValue = selectedTypeValue;
            window.UpdateSearchedTypes();
            var title = baseType != null
                ? $"Search Types Derived From {baseType?.Name} ({baseType.Namespace})"
                : "Search Types";
            window.titleContent = new GUIContent(title);
            window.ShowAuxWindow();
            return window;
        }

        private void OnGUI()
        {
            selectedStyle = selectedStyle ?? new GUIStyle("selectionRect");
            DrawMaxResultsWarningLayout();
            DrawSearchFieldLayout();

            BeginScrollView();
            foreach (var type in searchedTypes.Take(MaxResults))
            {
                DrawSelectableTypeLayout(type);
            }
            EndScrollView();
        }

        private void DrawMaxResultsWarningLayout()
        {
            if (searchedTypes.Length > MaxResults)
            {
                EditorGUILayout.LabelField(
                    $"only showing the first {MaxResults} of {searchedTypes.Length} results...",
                    EditorStyles.centeredGreyMiniLabel
                );
            }
        }

        private void DrawSearchFieldLayout()
        {
            var newSearchValue = EditorGUILayout.DelayedTextField(searchValue, EditorStyles.toolbarSearchField);
            if (newSearchValue != searchValue)
            {
                searchValue = newSearchValue;
                UpdateSearchedTypes();
            }
        }

        private void BeginScrollView()
        {
            scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);
        }

        private void DrawSelectableTypeLayout(Type type)
        {
            var typeValue = ToSerializedType(type);
            var buttonStyle = selectedTypeValue == ToSerializedType(type)
                ? selectedStyle
                : EditorStyles.objectField;

            var position = EditorGUILayout.BeginHorizontal();
            if (GUI.Button(position, GUIContent.none, buttonStyle))
            {
                Select(typeValue);
            }
            EditorGUILayout.LabelField(type.Name, EditorStyles.boldLabel);
            EditorGUILayout.LabelField($"({type.Namespace})");
            EditorGUILayout.EndHorizontal();
        }

        private void EndScrollView()
        {
            EditorGUILayout.EndScrollView();
        }

        private void UpdateSearchedTypes()
        {
            searchedTypes = (from type in GetTypes()
                             where !type.IsGenericType
                             where type.AssemblyQualifiedName.Contains(searchValue)
                             select type)
                             .ToArray();
        }

        private IEnumerable<Type> GetTypes()
        {
            return baseType != null
                ? TypeCache.GetTypesDerivedFrom(baseType)
                : CompilationPipeline.GetAssemblies()
                    .Select(assembly => assembly.name)
                    .Select(System.Reflection.Assembly.Load)
                    .SelectMany(a => a.GetExportedTypes());
        }

        private void Select(string typeValue)
        {
            selectedTypeValue = typeValue;
            onSelected?.Invoke(typeValue);
        }
    }
}
