﻿using System;
using System.Collections;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Reject.Extensions;
using static Reject.Serialization.SerializableType;
using static Reject.Serialization.SerializableTypeMeta;

namespace Reject.Serialization
{
    [CustomPropertyDrawer(typeof(SerializableType))]
    [CustomPropertyDrawer(typeof(DerivedFromAttribute))]
    public class SerializableTypeDrawer : PropertyDrawer
    {
        private static readonly GUIStyle pickButtonStyle = new GUIStyle("ObjectFieldButton");

        private Type baseType;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            baseType = GetBaseType(property);
            var serializedTypeProperty = property.FindPropertyRelative(SerializedTypePropertyName);

            position = EditorGUI.PrefixLabel(position, label);
            position = DrawTypeField(position, serializedTypeProperty);
            position = DrawPickTypeButton(position, serializedTypeProperty);
        }

        private Rect DrawTypeField(Rect position, SerializedProperty property)
        {
            var type = ToType(property.stringValue);
            position.width -= position.height;
            var label = type != null
                ? $"{type.Name} ({type.Namespace})"
                : baseType != null
                ? $"Type derived from {baseType.Name} ({baseType.Namespace})"
                : "Any Type";
            GUI.Button(position, label, EditorStyles.objectField);
            position.x += position.width;
            return position;
        }

        private Type GetBaseType(SerializedProperty property)
        {
            switch (attribute)
            {
                case DerivedFromAttribute d when d.Type != null:
                    return d.Type;
                case DerivedFromAttribute d when d.TypeDelegate != null:
                    return d.TypeDelegate();
                case DerivedFromAttribute d when d.TypeDelegateName != null:
                    d.TypeDelegate = CreateTypeDelegate(d.TypeDelegateName, property);
                    return d.TypeDelegate();
                default:
                    return null;
            }
        }

        private Func<Type> CreateTypeDelegate(string name, SerializedProperty property)
        {
            var parentObject = property.GetValue(path => path.SkipLast(1));

            return (Func<Type>)Delegate.CreateDelegate(
                typeof(Func<Type>),
                parentObject,
                name
            );
        }

        private Rect DrawPickTypeButton(Rect position, SerializedProperty property)
        {
            position.width = position.height;
            if (GUI.Button(position, GUIContent.none, pickButtonStyle))
            {
                SerializableTypeSearchWindow.ShowWindow(
                    baseType,
                    property.stringValue,
                    UpdateStringValue(property)
                );
            }
            position.x += position.width;
            return position;
        }

        private Action<string> UpdateStringValue(SerializedProperty property)
        {
            return value =>
            {
                property.stringValue = value;
                property.serializedObject.ApplyModifiedPropertiesWithoutUndo();
            };
        }
    }

    public class SerializableTypeMeta : SerializableType
    {
        public static string SerializedTypePropertyName = nameof(_serializedType);
    }
}
