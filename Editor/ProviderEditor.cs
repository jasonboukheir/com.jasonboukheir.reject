﻿using Reject.Extensions;
using UnityEditor;
using UnityEngine;

namespace Reject
{
    [CustomEditor(typeof(Provider))]
    public class ProviderEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            EditorGUILayout.Space();
            if (GUILayout.Button("Inject"))
            {
                var provider = (Provider)target;
                provider.Inject();
            }
        }
    }
}
